package ru.zak.game;

import java.util.Scanner;

/**
 * Простая игра основанная на угадывании чисел.
 * Пользователь должен угадать загаданное число введя его в консоль.
 * Если пользователь угадал число, то программа выведет «Правильно» и игра закончится, если нет, то пользователь продолжит вводить числа.
 * Вывести «Загаданное число больше»- если пользователь ввел число меньше загаданного, «Загаданное число меньше»- если пользователь ввел число больше загаданного.
 * author Заставская Анастасия 15oit18
 */
public class Game {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int number = (int) (Math.random() * 10);
        Game(number);

    }

    /**
     * Метод для сравнения введённого число с заданым рандомом.
     * Если пользователь угадывает число то ввыводиться сообщение "Вы угадали число", если нет "Попробуйте снова".
     * Если пользователь не угадывает число то выводится сообщение "Вы проиграли".
     * @param number-заданное число
     */

    public static void Game(int number) {
        System.out.println("Угадайте число");
        int n = scanner.nextInt();
        for (int i = 1; i < 2; i++) {
            if (n == number) {
                System.out.println("Вы угадали число");
            } else {
                System.out.println(" Вы не угадали осталось " + (3 - i) + " попытки");
                if (n > number) {
                    System.out.println("Число меньше введённого");
                } else {
                    System.out.println("Число больше введённого");
                }
                System.out.println("Попробуйте снова");
                n = scanner.nextInt();
            }
        }

        if (n != number) {
            System.out.println("У вас осталось последняя попытка°");
            if (n > number) {
                System.out.println("Число меньше введённого");
            } else {
                System.out.println("Число больше введённого");
            }
            System.out.println("Введите снова");
            n = scanner.nextInt();

        }

        if (n != number)

        {
            System.out.println("Вы проиграли");

        } else

        {
            System.out.println("Вы выйграли");

        }
    }
}

package ru.zak.Palindrom;
import java.util.Scanner;

/**
 * Напишите метод, который будет проверять является ли число палиндромом
 * (одинаково читающееся в обоих направлениях).
 *
 * @author Заставская
 */
public class Palindrom {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int number = sc.nextInt();

        String text = Integer.toString(number);

        if (check(text)==true){
            System.out.println("является");
        }else {
            System.out.println("не является");
        }
    }

    /**
     * Метод который проверяет являетя ли строка с числами палиндромом.
     *
     * @param convert строка с числамами.
     * @return результвт проверки
     */
    private static boolean check (String convert) {
        return convert.equals((new StringBuilder(convert)).reverse().toString());
    }
}
package ru.zak.prostoeChislo;

/**
 * Программа, которая выводит простые числа в пределах от 2 до 100.
 */
public class ProstoeChislo {


    public static void main(String[] args) {
        System.out.print("3, 5, 7, 9");

        for (int i = 2; i < 101; i++) {
            if (i%2!=0 && i%3!=0 && i%5!=0 && i%7!=0) {
                System.out.print(", " + i);
            }

        }

    }

}
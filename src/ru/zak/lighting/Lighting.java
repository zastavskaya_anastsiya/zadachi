package ru.zak.lighting;

import java.util.Scanner;

/**
 * Расчет расстояния до места удара молнии.
 * Зная интервал времени между вспышкой молнии и звуком сопровождающим ее можно рассчитать расстояние.
 * author Заставская Анастасия 15oit18
 */
public class Lighting {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double speed;
        double time;
        double distanse;

        System.out.println("Введите интервал: ");
        time = scanner.nextDouble();
        System.out.println("Введите скорость: ");
        speed = scanner.nextDouble();
        distanse = time * speed;
        System.out.println(distanse);


    }
}

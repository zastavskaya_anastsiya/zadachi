package ru.zak.multiplication;


import java.util.Scanner;

/**
 * Программа, которая выводит таблицу умножения для введенного пользователем числа с клавиатуры.
 * author Заставская Анастасия 15oit18
 */
public class Multiplication {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[]args){
        System.out.println("Р’РІРµРґРёС‚Рµ С‡РёР»Рѕ: ");
        int n = scanner.nextInt();
        Multiplic(n);

    }

    /**
     * Метод для вывода таблицы умножения для введённого пользователем числа
     * @param n-введённое пользователем число
     */

    public static void Multiplic(int n){
        for (int i = 1; i <= 10; i++) {
            int number = n*i;

            System.out.println(n + "*" + i +"="+number);

        }
    }
}

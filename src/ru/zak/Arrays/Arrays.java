package ru.zak.Arrays;

import java.util.Random;
import java.util.Scanner;

/**
 * Из двумерного массива заполненного случайными числами
 * перенесите построчно эти числа в одномерный массив.
 *
 * @author Заставская
 */
public class Arrays {
    static Scanner sc = new Scanner(System.in);
    static Random rm = new Random();


    public static void main(String[] args) {

        int[][] tyarray = new int[sc.nextInt()][sc.nextInt()];
        int[] array = new int[tyarray.length * tyarray[1].length];
        filling(tyarray);
        print(tyarray);
        copy(tyarray, array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");

        }
    }

    /**
     * Метод который переносит построчно элементы двухмерного массива в одномерный.
     *
     * @param tyarray двухмерный массив из которого нужно перенести элеменеты.
     * @param array массив в который нужной перенести элементы.
     */
    private static void copy(int[][] tyarray, int[] array) {
        for (int i = 0; i < tyarray.length; i++) {
            for (int j = 0; j < tyarray[i].length; j++) {
                array[i * tyarray[i].length + j] = tyarray[i][j];
            }
        }
    }

    /**
     * Метод заполняет двухмерный массив.
     *
     * @param tyarray массив.
     */

    private static void filling(int[][] tyarray) {
        for (int i = 0; i < tyarray.length; i++) {
            for (int j = 0; j < tyarray[i].length; j++) {
                tyarray[i][j] = rm.nextInt(30);
            }


        }
    }

    /**
     * Метод который выводит на экран элементы массива.
     *
     * @param tyarray двухмерный массив
     */
    private static void print (int[][] tyarray){
        for (int i = 0; i < tyarray.length; i++) {
            for (int j = 0; j < tyarray[i].length; j++) {
                System.out.print(tyarray[i][j] + " ");
            }
            System.out.println();


        }
    }
}

package ru.zak.Simple;
import java.util.Scanner;

/**
 * Напишите программу, которая выводит простые числа в пределах от 2 до 100.
 *
 * @author Заставская
 */
public class Simple {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int setmax = sc.nextInt();

        for (int i = 2; i < setmax; i++) {
            if (inspection(i)) {
                System.out.print(i + " ");
            }

        }

    }

    /**
     * Метод проверяет является ли число простым.
     *
     * @param number число.
     * @return реультат проверки.
     */
    private static boolean inspection(int number) {
        for (int i = 2; i * i <= number; i++)
            if (number % i == 0){
                return false;
            }
        return true;
    }

}
package ru.zak.Time;

import java.util.Scanner;

/**
 *Напишите программу, которая будет считать количество часов,
 *минут и секунд в n-ном количестве суток.
 *
 * @author Заставская
 */
public class Time {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int day = sc.nextInt();
        System.out.println("Кол-во часов в " + day + " сут. = " + (day * 24));
        System.out.println("Кол-во минут в "+ day + " сут. = "+ (day*24*60));
        System.out.println("Кол-во секунд в "+ day + " сут. = "+ (day*24*360)) ;


    }
}
